package org.example;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class UserService {

    public Set<User> getUsers() {

        return new HashSet<>(Arrays.asList(
                new User(1L, "John", "Wick"),
                new User(2L, "Bob", "Smith"),
                new User(3L, "Alice", "Ada"),
                new User(4L, "John", "Lennon")
                ));
    }

}

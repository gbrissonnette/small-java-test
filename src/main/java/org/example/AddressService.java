package org.example;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class AddressService {

    private final Map<Long, Address> userIdToAddressMap = new HashMap<>();

    public AddressService() {

        userIdToAddressMap.put(1L, new Address(1L, "Rue des Cèdres", "1920"));
        userIdToAddressMap.put(3L, new Address(2L, "Blancherie", "1950"));
    }

    public Optional<Address> findAddressFromUserId(Long userId){

        return Optional.ofNullable(userIdToAddressMap.get(userId));
    }
}

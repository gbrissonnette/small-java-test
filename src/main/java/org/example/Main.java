package org.example;

public class Main {

    /**
     * I want you to print on the console the address of every user
     * Be careful, not every users have an address
     * You can find the users using `UserService` and then ask for there address using
     * `AddressService`
     * 
     * Moreover, you can not change any class other than Main
     * 
     * if() statements or for() loops are not allowed
     * 
     */
    private static void printUserAddresses() {

    }


    public static void main(String[] args) {

        printUserAddresses();
    }
}